﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Management;
using HttpRequest;
using System.Net;
using System.Web;

namespace KhangQC
{

    public partial class Form1 : Form
    {
        bool run = true;
        string chienDich = "";
        string loDoSD = "";
        ChromeDriver chrome;
        private string myToken = "";
        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            
        }

        void LoadData()
        {
            Random randd = new Random();
            List<string> lisstLido = new List<string>();
            using (StreamReader rdd = new StreamReader("LydosudungPTTT.txt"))
            {
                string line = "";
                while ((line = rdd.ReadLine()) != null)
                {
                    lisstLido.Add(line);
                }
                
            }

            List<string> lisstCD = new List<string>();
            using (StreamReader rdd = new StreamReader("GiaithichngangonchiendichQC.txt"))
            {
                string line = "";
                while ((line = rdd.ReadLine()) != null)
                {
                    lisstCD.Add(line);
                }

            }
            chienDich = lisstCD[randd.Next(0, lisstCD.Count)];
            loDoSD = lisstLido[randd.Next(0,lisstLido.Count)];
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            button1.Enabled = false;
            comboBox3.Items.Add("3090");
            comboBox3.Items.Add("8879");
            comboBox3.SelectedIndex = 0;
            
            
        }
        List<string> listActivity;
        void IntilizationData()
        {

            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            ChromeOptions option = new ChromeOptions();
            option.AddArgument("--window-position=-32000,-32000");
            option.AddExtension("2.crx");
            chrome = new ChromeDriver(service, option);

            chrome.Navigate().GoToUrl("https://m.facebook.com/?cookielogin=true");
            //sent token to login
            chrome.FindElementById("token").SendKeys(dataGridView2[1, 0].Value.ToString());
            string status = "";
            //do
            //{
            //    chrome.FindElementByCssSelector("#LoginForm > fieldset:nth-child(1) > input[type=\"submit\"]:nth-child(3)").Click();
            //    Thread.Sleep(500);
            //    status = chrome.FindElementByCssSelector("#status").Text;
            //} while (status.Contains("failed"));
            chrome.FindElementByCssSelector("#LoginForm > fieldset:nth-child(1) > input[type=\"submit\"]:nth-child(3)").Click();
            Thread.Sleep(500);

            do
            {
                Thread.Sleep(400);
                status = chrome.FindElementByCssSelector("#status").Text;
                if (status.Contains("failed"))
                {
                    
                    Thread.Sleep(1000);
                    return;
                }

            } while (status.Contains("user_ID") == false||status.Contains("Chưa có tài khoản")==true);
          
            
            

            //chrome.Url = "https://www.facebook.com/ads/manage/powereditor/manage/campaigns";
            //chrome.FindElementByCssSelector("#ads_pe_container > div:nth-child(1) > div > div > div._ohe.lfloat > div > div._34_k > div > a").Click();
            //Thread.Sleep(2000);
            //string html = chrome.PageSource;
            //string result = Regex.Match(html, "Tài khoản cá nhân của bạn(.*?)accessible_elem layer_close_elem",RegexOptions.Multiline).Value;
            //var href = Regex.Matches(result, "href(.*?)>");
            //List<string> listID = new List<string>();
            //foreach(Match item in href)
            //{
            //    string a = Regex.Match(item.Value, "\\d{15}").Value;
            //    listID.Add(a);
            //}
            //using(StreamWriter writer=new StreamWriter("list.txt"))
            //{
            //    foreach (string item in listID)
            //    {
            //        writer.WriteLine(item);
            //    }
            //}
            //chrome.Url = "https://www.facebook.com/ads/manage/powereditor/manage/campaigns?act=" + listID[3];
            //chrome.Quit();
            chrome.Url = "https://www.facebook.com/help/contact/531795380173090";
            //chrome.FindElementById("SupportFormRow.1147314245324810").Click();

          

            //chrome.FindElementByLinkText("Chọn tài khoản quảng cáo").Click();
            chrome.FindElementByXPath("//*[@id=\"u_0_k\"]").Click();
            Thread.Sleep(200);
            string html = chrome.PageSource;
            string part = Regex.Match(html, "_54nq _57di _558b _2n_z(.*?)$").Value;
            var result = Regex.Matches(part, "_54nh(.*?)<");
            List<string> listAcc = new List<string>();

            foreach (Match item in result)
            {
                string vv = item.Value.Substring(7).Replace("<", "");
                listAcc.Add(vv);


            }
            listActivity=new List<string>();
            chrome.Url = "https://www.facebook.com/ads/manager/";
            string sourceHtml = chrome.PageSource;
            string big = Regex.Match(sourceHtml, "uiDataTable _15j9(.*?)</tbody>").Value;
            var medium = Regex.Matches(big,"href=(.*?)<");
            foreach (Match item in medium)
            {
                listActivity.Add(item.Value.Substring(51));
            }
            //Clipboard.SetText(sourceHtml);
            dataGridView1.Rows.Add(listAcc.Count);
            for (int i = 0; i < listAcc.Count; i++)
            {
                dataGridView1[1, i].Value = listAcc[i];
            }

            button1.Enabled = true;
            
            chrome.Quit();
            button5.Text = "Đăng xuất";
        }
        void ChangeStatusItems(int row,string sta)
        {
            
        }
        Thread t;
        private void button1_Click(object sender, EventArgs e)
        {

            //List<string> ll = new List<string>();
            //foreach (DataGridViewRow item in totalRows)
            //{
            //    if(Convert.ToBoolean(item.Cells[9].Value) == true)
            //    ll.Add(item.Cells[8].Value.ToString());
            //}

            //List<string> result = ll.Distinct().ToList<string>();
            //List<int> taikhoan = new List<int>();
            //foreach(var l in result)
            //{
            //    taikhoan.Add(Convert.ToInt32(l.Substring(2)));
            //}


            //int dem = 0;
            //int soLan = taikhoan[dem];


            //do
            //{
            //    Thread t = new Thread(new ParameterizedThreadStart(StartThread));
            //    t.Start((object)soLan);
            //    t.Join();
            //    soLan++;
            //    dem++;
            //} while (dem<taikhoan.Count);

            //t.IsBackground = true;
            t = new Thread(new ThreadStart(Khang));
            t.Start();


        }

        void Khang()
        {
            try
            {
                int maxThread = Convert.ToInt32(numericUpDown2.Value.ToString());
            bool rv = ThreadPool.SetMaxThreads(maxThread, maxThread);
            rv = ThreadPool.SetMinThreads(0, 0);
            LoadData();


            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[9].Value) == true)
                {
                    int tk = Convert.ToInt32(dataGridView1.Rows[i].Cells[8].Value.ToString().Substring(2));
                    dataGridView1[5, i].Value = "Sẵn sàng kháng!!!";
                    ThreadJob tj = new ThreadJob();
                    tj.SttOfList = i;
                    tj.LiDoSD = loDoSD;
                    tj.ChienDich = chienDich;
                    tj.Token = dataGridView2.Rows[tk - 1].Cells[1].Value.ToString();
                    tj.Name = dataGridView1[2, i].Value.ToString();
                    tj.onstatusChange += new ThreadJob.ThreadEventHandle(tj_onStatusChange);
                    ThreadPool.QueueUserWorkItem(new WaitCallback(tj.DoingThread), null);
                }

            }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void tj_onStatusChange(ThreadJob sender)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                //int totalWorkerThreads = 0;
                //int totalCompletePort = 0;
                //ThreadPool.GetAvailableThreads(out totalWorkerThreads, out totalCompletePort);
                if (sender.Status.Equals("Đang tạo kháng quảng cáo!!!")) dataGridView1.Rows[sender.SttOfList].DefaultCellStyle.BackColor = Color.Yellow;
                else if (sender.Status.Equals("Đã Gửi Yêu Cầu Kháng Thành Công!!!"))
                {
                    dataGridView1.Rows[sender.SttOfList].DefaultCellStyle.BackColor = Color.Green;
                    dataGridView1.Rows[sender.SttOfList].Cells[9].Value = false;
                    Increase();
                }  
                else if (sender.Status.Equals("Đã có lỗi sảy ra")) dataGridView1.Rows[sender.SttOfList].DefaultCellStyle.BackColor = Color.Red;
                dataGridView1.Rows[sender.SttOfList].Cells[5].Value = sender.Status;
            }));

        }

        void Increase()
        {
            label13.Invoke(new MethodInvoker(delegate
            {
                int solan = Convert.ToInt32(label3.Text);
                solan--;
                label3.Text = "" + solan;
            }));

            label11.Invoke(new MethodInvoker(delegate
            {
                int success = Convert.ToInt32(label11.Text);
                success++;
                label11.Text = "" + success;
            }));

            
        }

        int countSuccess = 0;
        void StartThread(object to)
        {

            int counttt = (int)to;

            string url = "https://b-api.facebook.com/method/auth.getSessionForApp?format=json&access_token="+ dataGridView2.Rows[counttt - 1].Cells[1].Value.ToString()+"&new_app_id=350685531728&generate_session_cookies=1&__mref=message_bubble";
            RequestHTTP request = new RequestHTTP();
            string login1 = request.Request("GET", url);
            var json_serializer = new JavaScriptSerializer();
            var obj = json_serializer.Deserialize<dynamic>(login1);
            OpenQA.Selenium.Cookie cok = new OpenQA.Selenium.Cookie(obj["session_cookies"][0]["name"], obj["session_cookies"][0]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
            OpenQA.Selenium.Cookie cok1 = new OpenQA.Selenium.Cookie(obj["session_cookies"][1]["name"], obj["session_cookies"][1]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
            OpenQA.Selenium.Cookie cok2 = new OpenQA.Selenium.Cookie(obj["session_cookies"][2]["name"], obj["session_cookies"][2]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
            OpenQA.Selenium.Cookie cok3 = new OpenQA.Selenium.Cookie(obj["session_cookies"][3]["name"], obj["session_cookies"][3]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
            ChromeDriver chrome1 = new ChromeDriver();
            chrome1.Navigate().GoToUrl("https://www.facebook.com/");
            chrome1.Manage().Cookies.AddCookie(cok);
            chrome1.Manage().Cookies.AddCookie(cok1);
            chrome1.Manage().Cookies.AddCookie(cok2);
            chrome1.Manage().Cookies.AddCookie(cok3);

            //ChromeDriver chrome1;
            //ChromeOptions option1 = new ChromeOptions();
            //option1.AddArgument("--window-position=-32000,-32000");
            //option1.AddExtension("2.crx");
            //ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            //service.HideCommandPromptWindow = true;
            //chrome1 = new ChromeDriver(service, option1);

            //chrome1.Navigate().GoToUrl("https://m.facebook.com/?cookielogin=true");
            //Thread.Sleep(500);
            //chrome1.FindElementById("token").SendKeys(dataGridView2.Rows[counttt-1].Cells[1].Value.ToString());
            //chrome1.FindElementByCssSelector("#LoginForm > fieldset:nth-child(1) > input[type=\"submit\"]:nth-child(3)").Click();

            chrome1.Url = "https://www.facebook.com/help/contact/531795380173090";
            IJavaScriptExecutor js = chrome1 as IJavaScriptExecutor;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {

                    if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[9].Value) == true)
                    {
                    if(dataGridView1.Rows[i].Cells[8].Value.ToString()==("tk"+counttt))
                    {
                        dataGridView1[5, i].Value = "Đang tạo kháng quảng cáo!!!";
                        dataGridView1.Update();
                        var lines = File.ReadAllLines("data.txt");
                        var r = new Random();
                        var randomLineNumber = r.Next(0, lines.Length - 1);


                        string firstLine = lines[randomLineNumber];

                        string[] dataRow = firstLine.Split('|');

                        Thread.Sleep(200);

                        //try
                        //{
         
                            js.ExecuteScript(Parameter(dataRow[0], "451481694899640"));
                            js.ExecuteScript(Parameter(dataRow[1], "413349935407960"));


                            js.ExecuteScript(Parameter(loDoSD.Substring(3), "162516177522246"));
                            js.ExecuteScript(Parameter(chienDich.Substring(3), "474923745899216"));

                            Thread.Sleep(100);
                            js.ExecuteScript(Parameter("Vietnam", "u_0_m"));
                            js.ExecuteScript(Parameter("Vietnam", "u_0_n"));
                            //radioButton
                            js.ExecuteScript(Parameter("Yes", "1465140640464740.0"));

                            js.ExecuteScript(Parameter("Yes", "484697378367177.0"));

                            js.ExecuteScript(Parameter("Yes", "1465140640464740.0"));

                            js.ExecuteScript(Parameter("Yes", "484697378367177.0"));


                            js.ExecuteScript(Checked("true", "1465140640464740.0"));
                            js.ExecuteScript(Checked("true", "484697378367177.0"));
                            js.ExecuteScript(Checked("true", "675774322578380.0"));
                            js.ExecuteScript(Checked("true", "942684689210663.4"));
                            js.ExecuteScript(Checked("true", "453855151341120.1"));
                            js.ExecuteScript(Checked("true", "328306270852173.1"));
                            js.ExecuteScript(Checked("true", "1162636333792958.1"));


                            //upload image
                            chrome1.FindElementByName("ID[]").SendKeys(Path.GetFullPath("data Image")+"\\"+dataRow[2]);
                            //ID[]").SendKeys(@"C:\Users\NDT\Desktop\ToolKhangFB\cmnd\cmnd2.jpg
                            //(Application.ExecutablePath) + @"\data Image\" + dataRow[2]
                            dataGridView1[5, i].Value = "Đã Gửi Yêu Cầu Kháng Thành Công!!!";
                            dataGridView1.Update();
                            Thread.Sleep(4000);
                            //js.ExecuteScript("document.getElementById(\"u_0_5\").click();");
                            countSuccess++;
                            label11.Text = "" + countSuccess;
                            Thread.Sleep(Convert.ToInt32(numericUpDown1.Value));
                            dataGridView1[9, i].Value = false;
                            int solan = Convert.ToInt32(label3.Text);
                            solan--;
                            label3.Text = "" + solan;
                            using(StreamWriter wt=new StreamWriter("ListSent.txt",true))
                            {
                                DateTime time = DateTime.Now;
                                wt.WriteLine(dataGridView1[1,i].Value.ToString()+"|"+time.ToString());
                            }
                        //}
                        //catch(Exception ex)
                        //{
                        //    MessageBox.Show(ex.ToString());
                        //}
                    }
                        
                    }
                }
                //Thread.Sleep(1500);
                //chrome1.Quit();         
            }
                    
  
        


        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                int count = Convert.ToInt32(label3.Text);
                if (Convert.ToBoolean(dataGridView1.CurrentRow.Cells[9].Value) == true)
                {
                    dataGridView1.CurrentRow.Cells[9].Value = false;
                    count--;
                }

                else
                {
                    dataGridView1.CurrentRow.Cells[9].Value = true;
                    count++;
                }
                label3.Text = "" + count;
            }
            catch { };
            

        }


        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            //Thread t = new Thread(new ThreadStart(LoadTK));
            //t.Start();
            //ThreadPool.QueueUserWorkItem(new WaitCallback(LoadTK));
            LoadTK();
    }

        void LoadTK()
        {
            List<string> lisstSent = new List<string>();
            using (StreamReader rdd = new StreamReader("ListSent.txt"))
            {
                string line = "";
                while ((line = rdd.ReadLine()) != null)
                {
                    lisstSent.Add(line);
                }

            }
            dataGridView1.Rows.Clear();

            
            for (int j = 0; j < dataGridView2.Rows.Count; j++)
            {
                if (Convert.ToBoolean(dataGridView2.Rows[j].Cells[4].Value) == true)
                {

                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {

                            string html = client.GetStringAsync("https://graph.facebook.com/v2.11/me/adaccounts?access_token=" + dataGridView2[1, j].Value.ToString() + "&_reqName=me%2Fadaccounts&_reqSrc=AdsTypeaheadDataManager&_sessionID=66ab2aeaa6c9b788&fields=[%22account_id%22%2C%22name%22%2C%22amount_spent%22%2C%22account_status%22%2C%22funding_source_details%22%2C%22currency%22,\"timezone_name\"]&include_headers=false&limit=30000&locale=vi_VN&method=get&pretty=0&sort=name_ascending&suppress_http_code=1").Result;

                            Thread.Sleep(500);
                            var json_serializer = new JavaScriptSerializer();
                            //Account acc = json_serializer.Deserialize<Account>(html);
                            var obj = json_serializer.Deserialize<dynamic>(html);

                            for (int i = 0; i < obj["data"].Length; i++)
                            {
                                List<string> lil = lisstSent.Where(it => it.Split('|')[0] == obj["data"][i]["account_id"]).ToList();
                                if (lil.Count > 0)
                                {
                                        string stt = "";
                                    if (obj["data"][i]["account_status"] == 2) stt = "Hoạt Động bất thường" + "(Kháng " + lil[0].Split('|')[1] + ")";
                                    else
                                        if (obj["data"][i]["account_status"] == 1) stt = "Đang hoạt động";
                                    else
                                        stt = "Ngừng hoạt động";

                                    dataGridView1.Rows.Add(dataGridView1.Rows.Count + 1, obj["data"][i]["account_id"], obj["data"][i]["name"], "", obj["data"][i]["amount_spent"], stt, obj["data"][i]["currency"], obj["data"][i]["timezone_name"], "tk" + (j + 1));
                                }
                                else
                                {
                                    string stt = "";
                                    if (obj["data"][i]["account_status"] == 2) stt = "Hoạt Động bất thường";
                                    else
                                        if (obj["data"][i]["account_status"] == 1) stt = "Đang hoạt động";
                                    else
                                        stt = "Ngừng hoạt động";

                                    dataGridView1.Rows.Add(dataGridView1.Rows.Count + 1, obj["data"][i]["account_id"], obj["data"][i]["name"], "", obj["data"][i]["amount_spent"], stt, obj["data"][i]["currency"], obj["data"][i]["timezone_name"], "tk" + (j + 1));
                                }


                            }
                        }
                    }
                    catch
                    {
                        //MessageBox.Show("Đã có lỗi sảy ra!!!");
                        int totalWorkerThreads = 0;
                        int totalCompletePort = 0;
                        ThreadPool.GetAvailableThreads(out totalWorkerThreads, out totalCompletePort);
                        totalWorkerThreads++;
                        ThreadPool.SetMaxThreads(totalWorkerThreads, totalWorkerThreads);
                        LoadTK();
                    }

                    totalRows.Clear();
                    for (int i = 0; i < dataGridView1.RowCount; i++)
                    {
                        totalRows.Add(dataGridView1.Rows[i]);
                    }

                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime current = DateTime.Now;
            DateTime user = dateTimePicker1.Value;
            if (current.Hour == user.Hour && current.Minute == user.Minute && current.Day == user.Day)
            {

                try
                {
                    List<string> ll = new List<string>();
                    foreach (DataGridViewRow item in totalRows)
                    {
                        if (Convert.ToBoolean(item.Cells[9].Value) == true)
                            ll.Add(item.Cells[8].Value.ToString());
                    }

                    List<string> result = ll.Distinct().ToList<string>();
                    List<int> taikhoan = new List<int>();
                    foreach (var l in result)
                    {
                        taikhoan.Add(Convert.ToInt32(l.Substring(2)));
                    }


                    int dem = 0;
                    int soLan = taikhoan[dem];


                    do
                    {
                        Thread t = new Thread(new ParameterizedThreadStart(StartThread));
                        t.Start((object)soLan);
                        t.Join();
                        soLan++;
                        dem++;
                    } while (dem < taikhoan.Count);
                }
                catch
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            if (button6.Text == "Hẹn Giờ")
            {
                button6.Text = "Huỷ Bỏ";
                timer1.Enabled = true;
            }
            else if (button6.Text == "Huỷ Bỏ")
            {
                button6.Text = "Hẹn Giờ";
                timer1.Enabled = false;
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.Items.Add("Chọn Tất Cả");
            List<string> list = new List<string>();
            for(int i = 0; i < dataGridView1.Rows.Count;i++)
            {
                list.Add(dataGridView1[6, i].Value.ToString());
            }
            List<string> result = list.Distinct().ToList<string>();
            foreach(var item in result)
            {
                comboBox1.Items.Add(item);
            }

            
        }
        List<DataGridViewRow> totalRows = new List<DataGridViewRow>();
        private void button2_Click(object sender, EventArgs e)
        {
            if(comboBox1.Text== "Chọn Tất Cả")
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < totalRows.Count; i++)
                {
                    dataGridView1.Rows.Add(totalRows[i]);
                }
                return;
            }

            if (comboBox2.Text == "Chọn Tất Cả")
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < totalRows.Count; i++)
                {
                    dataGridView1.Rows.Add(totalRows[i]);
                }
                return;
            }
            if (comboBox2.Text.Equals("Đã Gửi Y/C Kháng"))
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < totalRows.Count; i++)
                {
                    if (totalRows[i].Cells[5].Value.ToString().Contains("(")|| comboBox2.Text.Equals("Đã Gửi Yêu Cầu Kháng Thành Công!!!"))
                    dataGridView1.Rows.Add(totalRows[i]);
                }
                return;
            }
            if (textBox1.Text == "")
            {

                if (comboBox1.Text != "" && comboBox2.Text != "")
                {
                    List<DataGridViewRow> list = new List<DataGridViewRow>();
                    for (int i = 0; i < totalRows.Count; i++)
                    {
                        list.Add(totalRows[i]);
                    }
                    dataGridView1.Rows.Clear();
                    foreach (var items in list)
                    {
                        if (items.Cells[6].Value.ToString() == comboBox1.Text && items.Cells[5].Value.ToString() == comboBox2.Text)
                        {
                            dataGridView1.Rows.Add(items);
                        }
                    }
                }
                else if (comboBox1.Text != "" || comboBox2.Text != "")
                {
                    if (comboBox1.Text != "")
                    {
                        List<DataGridViewRow> list = new List<DataGridViewRow>();
                        for (int i = 0; i < totalRows.Count; i++)
                        {
                            list.Add(totalRows[i]);
                        }
                        dataGridView1.Rows.Clear();
                        foreach (var items in list)
                        {
                            if (items.Cells[6].Value.ToString() == comboBox1.Text)
                            {
                                dataGridView1.Rows.Add(items);
                            }
                        }
                    }
                    else
                    {
                        List<DataGridViewRow> list = new List<DataGridViewRow>();
                        for (int i = 0; i < totalRows.Count; i++)
                        {
                            list.Add(totalRows[i]);
                        }
                        dataGridView1.Rows.Clear();
                        foreach (var items in list)
                        {
                            if (items.Cells[5].Value.ToString() == comboBox2.Text)
                            {
                                dataGridView1.Rows.Add(items);
                            }
                        }
                    }
                }

            }else
            {
                List<DataGridViewRow> list = new List<DataGridViewRow>();
                for (int i = 0; i < totalRows.Count; i++)
                {
                    list.Add(totalRows[i]);
                }
                dataGridView1.Rows.Clear();
                foreach (var items in list)
                {
                    if (items.Cells[1].Value.ToString() ==textBox1.Text)
                    {
                        dataGridView1.Rows.Add(items);
                    }
                }
            }   

            //DataView view = (DataView)dataGridView1.DataSource;
            //view.RowFilter = "Tiền='USD'";
            //dataGridView1.DataSource = view;


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_DropDown(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            comboBox2.Items.Add("Đã Gửi Y/C Kháng");
            comboBox2.Items.Add("Chọn Tất Cả");
            List<string> list = new List<string>();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                list.Add(dataGridView1[5, i].Value.ToString());
            }
            List<string> result = list.Distinct().ToList<string>();
            List<string> rr = result.Where(d => d.Contains("(") == false).ToList();
            foreach (var item in rr)
            {
                comboBox2.Items.Add(item);
            }
        }


        private void button7_Click_1(object sender, EventArgs e)
        {
            try
            {
                LoadToken();
            }
            catch
            {
                int totalWorkerThreads = 0;
                int totalCompletePort = 0;
                ThreadPool.GetAvailableThreads(out totalWorkerThreads, out totalCompletePort);
                totalWorkerThreads++;
                ThreadPool.SetMaxThreads(totalWorkerThreads, totalWorkerThreads);
                LoadToken();

            }
            
        }

        void LoadToken()
        {
            dataGridView2.Rows.Clear();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string line;
                List<string> li = new List<string>();
                string path = openFileDialog1.FileName;
                StreamReader rd = new StreamReader(path);
                while ((line = rd.ReadLine()) != null)
                {
                    li.Add(line);
                }
                for (int i = 0; i < li.Count; i++)
                {
                    dataGridView2.Rows.Add(i + 1, li[i]);
                }
                for (int i = 0; i < dataGridView2.Rows.Count; i++)
                {
                    HttpClient client = new HttpClient();
                    string html = client.GetStringAsync("https://graph.facebook.com/v2.11/me/albums?access_token=" + dataGridView2[1, i].Value.ToString()).Result;
                    var json_serializer = new JavaScriptSerializer();
                    //Account acc = json_serializer.Deserialize<Account>(html);
                    var obj = json_serializer.Deserialize<dynamic>(html);

                    if (html.Contains("data"))
                    {

                        dataGridView2[3, i].Value = " Đăng nhập thành công";
                        for (int t = 0; t < obj["data"].Length; t++)
                        {
                            if (obj["data"][t]["name"] == "Profile Pictures")
                            {
                                dataGridView2[2, i].Value = obj["data"][t]["created_time"];
                            }

                        }
                    }
                    else
                    {
                        dataGridView2[3, i].Value = "đăng nhập thất bại";
                    }

                }
            }
        }

        private void dataGridView2_Click(object sender, EventArgs e)
        {
            try
            {
                //int count = Convert.ToInt32(label3.Text);
                if (Convert.ToBoolean(dataGridView2.CurrentRow.Cells[4].Value) == true)
                {
                    dataGridView2.CurrentRow.Cells[4].Value = false;
                    //count--;
                }
                else
                {
                    dataGridView2.CurrentRow.Cells[4].Value = true;
                    //count++;
                }
                //label3.Text = "" + count;
            }
            catch(Exception ex) { MessageBox.Show(ex.ToString()); };
        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                if (dataGridView1.Columns[9].HeaderText == "Chọn")
                {
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        dataGridView1[9, i].Value = true;
                    }
                    label3.Text = "" + (dataGridView1.Rows.Count);
                    dataGridView1.Columns[9].HeaderText = "Hủy";
                }
                else if (dataGridView1.Columns[9].HeaderText == "Hủy")
                {
                    dataGridView1.Columns[9].HeaderText ="Chọn";
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        dataGridView1[9, i].Value = false;
                    }
                    label3.Text = "" + 0;
                }
            }
        }

        string Parameter(string para, string id)
        {
            string s = "document.getElementById(\"474923745899216\").value=\"hello\"";
            string[] a = s.Split('\"');
            string chuoi = s.Replace(a[1], id).Replace(a[3],para);
            return chuoi;
        }

        string Checked(string val,string id)
        {
            string s= "document.getElementById(\"daylaid\").checked ="+val;
            string[] a = s.Split('\"');
            string chuoi = s.Replace("daylaid", id);
            return chuoi;
        }
        string tokenn = "";
        private void button3_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[9].Value) == true)
                {
                    int tk = Convert.ToInt32(dataGridView1.Rows[i].Cells[8].Value.ToString().Substring(2));
                    string url = "https://b-api.facebook.com/method/auth.getSessionForApp?format=json&access_token=" + dataGridView2.Rows[tk-1].Cells[1].Value.ToString() + "&new_app_id=350685531728&generate_session_cookies=1&__mref=message_bubble";
                    RequestHTTP request = new RequestHTTP();
                    string login1 = request.Request("GET", url);
                    var json_serializer = new JavaScriptSerializer();
                    //Account acc = json_serializer.Deserialize<Account>(html);
                    var obj = json_serializer.Deserialize<dynamic>(login1);


                    System.Net.Cookie cok = new System.Net.Cookie(obj["session_cookies"][0]["name"], obj["session_cookies"][0]["value"]) { Domain = ".facebook.com" };
                    System.Net.Cookie cok1 = new System.Net.Cookie(obj["session_cookies"][1]["name"], obj["session_cookies"][1]["value"]) { Domain = ".facebook.com" };
                    System.Net.Cookie cok2 = new System.Net.Cookie(obj["session_cookies"][2]["name"], obj["session_cookies"][2]["value"]) { Domain = ".facebook.com" };
                    System.Net.Cookie cok3 = new System.Net.Cookie(obj["session_cookies"][3]["name"], obj["session_cookies"][3]["value"]) { Domain = ".facebook.com" };
                    CookieCollection cookies = new CookieCollection();
                    cookies.Add(cok);
                    cookies.Add(cok1);
                    cookies.Add(cok2);
                    cookies.Add(cok3);
                    //RequestHTTP request = new RequestHTTP();
                    request.SetDefaultHeaders(new string[] { "User-Agent: " + "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/15.0" });
                    request.AddRequestCookies(cookies);
                    //string homeFace = request.Request("POST", "https://www.facebook.com/ajax/bz",null, Encoding.UTF8.GetBytes("__a=1&__be=1&__dyn=7xe6Fo4OQ1IKEK4oy4--C1swgE98nwgU6C7UW8xi6obUuwmE4i3K4o5K0Y8hwem0nCqU4G0OE2WxO2u1-G2-2W2y&__pc=PHASED%3ADEFAULT&__req=3&__rev=3890182&__spin_b=trunk&__spin_r=3890182&__spin_t=1525853202&__user=100004995669107&fb_dtsg=AQFVm_o6XUPT%3AAQH5HOe1YnHG&jazoest=2658170861099511154888580845865817253727910149891107271&ph=C3&q=%5B%7B%22user%22%3A%22100004995669107%22%2C%22page_id%22%3A%22gu0x25%22%2C%22posts%22%3A%5B%5B%22click_ref_logger%22%2C%5B%2205aP%22%2C1525853284216%2C%22act%22%2C1525853284209%2C0%2C%22ID%5B%5D%22%2C%22click%22%2C%22click%22%2C%22-%22%2C%22r%22%2C%22%2Fhelp%2Fcontact%2F531795380173090%22%2C%7B%22ft%22%3A%7B%7D%2C%22gt%22%3A%7B%7D%7D%2C0%2C0%2C0%2C0%2C%22gu0x25%22%2C%22WebSupportFormController%22%5D%2C1525853284216%2C0%5D%5D%2C%22trigger%22%3A%22click_ref_logger%22%2C%22send_method%22%3A%22ajax%22%7D%5D&ts=1525853284324"));
                    string homeFace = request.Request("GET", "https://www.facebook.com/support");

                    //string s = Uri.UnescapeDataString(homeFace);
                    string contentMessage0 = Regex.Match(homeFace, dataGridView1.Rows[i].Cells[1].Value.ToString() + "(.*?)content\":{\"__html\":\"(.*?)}").Value;
                    if (contentMessage0.Length > 6000)
                    {
                        dataGridView1.Rows[i].Cells[3].Value = "Chưa có có thư!!!";
                    }
                    else
                    {
                        try
                        {
                            string contentMessage = Regex.Match(contentMessage0, "content\":{\"__html\":\"(.*?)}").Value.Substring(9);
                            var json = new JavaScriptSerializer();
                            //Account acc = json_serializer.Deserialize<Account>(html);
                            var doituong = json.Deserialize<dynamic>(contentMessage);
                            string ib = doituong["__html"];
                            string ib1 = ib.Substring(5);
                            //string ib1 = Regex.Match(ib.Substring(5), "").Value;

                            //textBox2.Text = ib1.Replace("<br />", "");
                            dataGridView1.Rows[i].Cells[3].Value = ib1.Replace("<br />","");
                        }
                        catch
                        {
                            dataGridView1.Rows[i].Cells[3].Value = "Chưa có có thư!!!";
                        }

                    }
                }
            }

            
            
            //System.Web.Helpers.Json.Decode

            //for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //{
            //    if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[9].Value) == true)
            //    {
            //        ThreadCheck c = new ThreadCheck();
            //        c.Stt = i;
            //        int tk = Convert.ToInt32(dataGridView1.Rows[i].Cells[8].Value.ToString().Substring(2));
            //        c.Token= dataGridView2.Rows[tk - 1].Cells[1].Value.ToString();
            //        c.CheckIBStatusChange += new ThreadCheck.ThreadCheckIB(IBStatusChange);

            //        ThreadPool.QueueUserWorkItem(new WaitCallback(c.CheckIB), null);
            //    }
        }

        private void IBStatusChange(ThreadCheck sender)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                dataGridView1.Rows[sender.Stt].Cells[3].Value = sender.Status;
            }));
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng này chưa có,chúng tôi sẽ update sớm nhất !!!");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhangQC
{
    class Account
    {
        private string id;
        private string name;
        private int amountSpent;
        private int status;
        private string currency;
        private string id1;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int AmountSpent { get => amountSpent; set => amountSpent = value; }
        public int Status { get => status; set => status = value; }
        public string Currency { get => currency; set => currency = value; }
        public string Id1 { get => id1; set => id1 = value; }
    }
}

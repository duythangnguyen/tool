﻿using HttpRequest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace KhangQC
{
    class ThreadJob
    {
        private int tk;
        private string liDoSD;
        private string chienDich;
        private string token;
        private string status;
        private string name;
        private int sttOfList;

        public int Tk { get => tk; set => tk = value; }
        public string LiDoSD { get => liDoSD; set => liDoSD = value; }
        public string ChienDich { get => chienDich; set => chienDich = value; }
        public string Token { get => token; set => token = value; }
        public string Status { get => status; set => status = value; }
        public string Name { get => name; set => name = value; }
        public int SttOfList { get => sttOfList; set => sttOfList = value; }

        public delegate void ThreadEventHandle(ThreadJob  sender);
        public event ThreadEventHandle onstatusChange;
        //public  ThreadJob(int tk,string liDoSD,string chienDich,string token)
        //{
        //    this.tk = tk;
        //    this.liDoSD = liDoSD;
        //    this.chienDich = chienDich;
        //    this.token = token;
        //}

        public void DoingThread(object to)
        {
            try
            {
                this.Status = "Đang tạo kháng quảng cáo!!!"; onstatusChange(this);
                string url = "https://b-api.facebook.com/method/auth.getSessionForApp?format=json&access_token=" + Token + "&new_app_id=350685531728&generate_session_cookies=1&__mref=message_bubble";
                RequestHTTP request = new RequestHTTP();
                string login1 = request.Request("GET", url);
                var json_serializer = new JavaScriptSerializer();
                var obj = json_serializer.Deserialize<dynamic>(login1);
                Cookie cok = new Cookie(obj["session_cookies"][0]["name"], obj["session_cookies"][0]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
                Cookie cok1 = new Cookie(obj["session_cookies"][1]["name"], obj["session_cookies"][1]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
                Cookie cok2 = new Cookie(obj["session_cookies"][2]["name"], obj["session_cookies"][2]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
                Cookie cok3 = new Cookie(obj["session_cookies"][3]["name"], obj["session_cookies"][3]["value"], ".facebook.com", "/", DateTime.Now.AddDays(1));
                ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;

                ChromeDriver chrome1 = new ChromeDriver(service);
                chrome1.Manage().Window.Size = new Size(550, 700);

                chrome1.Navigate().GoToUrl("https://www.facebook.com/");
                chrome1.Manage().Cookies.AddCookie(cok);
                chrome1.Manage().Cookies.AddCookie(cok1);
                chrome1.Manage().Cookies.AddCookie(cok2);
                chrome1.Manage().Cookies.AddCookie(cok3);

                //ChromeDriver chrome1;
                //ChromeOptions option1 = new ChromeOptions();
                //option1.AddArgument("--window-position=-32000,-32000");
                //option1.AddExtension("2.crx");
                //ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                //service.HideCommandPromptWindow = true;
                //chrome1 = new ChromeDriver(service, option1);

                //chrome1.Navigate().GoToUrl("https://m.facebook.com/?cookielogin=true");
                //Thread.Sleep(500);
                //chrome1.FindElementById("token").SendKeys(dataGridView2.Rows[counttt-1].Cells[1].Value.ToString());
                //chrome1.FindElementByCssSelector("#LoginForm > fieldset:nth-child(1) > input[type=\"submit\"]:nth-child(3)").Click();

                chrome1.Url = "https://www.facebook.com/help/contact/531795380173090";
                IJavaScriptExecutor js = chrome1 as IJavaScriptExecutor;

                try
                {

                    var lines = File.ReadAllLines("data.txt");
                    var r = new Random();
                    var randomLineNumber = r.Next(0, lines.Length - 1);


                    string firstLine = lines[randomLineNumber];

                    string[] dataRow = firstLine.Split('|');

                    js.ExecuteScript(Parameter(dataRow[0], "451481694899640"));
                    js.ExecuteScript(Parameter(dataRow[1], "413349935407960"));


                    js.ExecuteScript(Parameter(liDoSD.Substring(3), "162516177522246"));
                    js.ExecuteScript(Parameter(ChienDich.Substring(3), "474923745899216"));

                    Thread.Sleep(100);
                    js.ExecuteScript(Parameter("Vietnam", "u_0_m"));
                    js.ExecuteScript(Parameter("Vietnam", "u_0_n"));
                    //radioButton
                    js.ExecuteScript(Parameter("Yes", "1465140640464740.0"));

                    js.ExecuteScript(Parameter("Yes", "484697378367177.0"));

                    js.ExecuteScript(Parameter("Yes", "1465140640464740.0"));

                    js.ExecuteScript(Parameter("Yes", "484697378367177.0"));


                    js.ExecuteScript(Checked("true", "1465140640464740.0"));
                    js.ExecuteScript(Checked("true", "484697378367177.0"));
                    js.ExecuteScript(Checked("true", "675774322578380.0"));
                    js.ExecuteScript(Checked("true", "942684689210663.4"));
                    js.ExecuteScript(Checked("true", "453855151341120.1"));
                    js.ExecuteScript(Checked("true", "328306270852173.1"));
                    js.ExecuteScript(Checked("true", "1162636333792958.1"));


                    //upload image
                    chrome1.FindElementByName("ID[]").SendKeys(Path.GetFullPath("data Image") + "\\" + dataRow[2]);

                    //ID[]").SendKeys(@"C:\Users\NDT\Desktop\ToolKhangFB\cmnd\cmnd2.jpg
                    //(Application.ExecutablePath) + @"\data Image\" + dataRow[2]
                    chrome1.FindElementByXPath("//*[@id=\"u_0_k\"]").Click();
                    chrome1.FindElementByLinkText(Name).Click();


                    Thread.Sleep(3500);

                    js.ExecuteScript("document.getElementById(\"u_0_o\").click();");
                    this.Status = "Đã Gửi Yêu Cầu Kháng Thành Công!!!"; onstatusChange(this);
                }
                catch
                {
                    this.Status = "Đã có lỗi sảy ra"; onstatusChange(this);

                }

                Thread.Sleep(1500);
                chrome1.Quit();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        string Parameter(string para, string id)
        {
            string s = "document.getElementById(\"474923745899216\").value=\"hello\"";
            string[] a = s.Split('\"');
            string chuoi = s.Replace(a[1], id).Replace(a[3], para);
            return chuoi;
        }

        string Checked(string val, string id)
        {
            string s = "document.getElementById(\"daylaid\").checked =" + val;
            string[] a = s.Split('\"');
            string chuoi = s.Replace("daylaid", id);
            return chuoi;
        }
    }
}

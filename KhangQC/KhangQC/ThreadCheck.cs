﻿using HttpRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace KhangQC
{
    class ThreadCheck
    {
        private string status;
        private string token;
        private int stt;
        public delegate void ThreadCheckIB(ThreadCheck sender);
        public event ThreadCheckIB CheckIBStatusChange;

        public string Status { get => status; set => status = value; }
        public string Token { get => token; set => token = value; }
        public int Stt { get => stt; set => stt = value; }

        public void CheckIB(object tb)
        {
            this.Status = "Đang kiểm tra phản hồi...";CheckIBStatusChange(this);

           
        }
    }
}
